Checkbook Math
==============

A small library providing utility methods for simple checkbook math.

## Installation

  npm install checkbook --save

## Usage
  
  var checkbook = require('checkbook'),
    sum = checkbook.sum,
    dif = checkbook.dif;
    
  var a = 3,
      b = 2,
      summation = sum(a,b),
      difference = dif(a,b);

  console.log(a+'+'+b, summation, a+'-'+b, difference);
  
## Tests

  npm test

## Contributing

In lieu of a formal styleguide, take care to maintain the existing coding style.
Add unit tests for any new or changed functionality. Lint and test your code.

## Release History

* 0.1.0 Initial release