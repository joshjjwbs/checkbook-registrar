var should = require('chai').should(),
    checkbook = require('../index'),
    sum = checkbook.sum,
    dif = checkbook.dif;

describe('#sum', function() {
  it('adds 2 and 3 to equal 5', function() {
    sum(2,3).should.equal(5);
  });
});

describe('#dif', function() {
  it('finds the difference between 5 and 3 to equal 2', function() {
    dif(5,3).should.equal(2);
  });
});