/**
 * Find the SUM of 2 given numbers
 * 
 * @param {number} a
 * @param {number} b
 * @return {number} sum
 */
module.exports = {
  sum: function(a,b){
    return Number(a + b);
  },
  /**
   * Find the DIFFERENCE (dif) of 2 given numbers
   * @param {number} a
   * @param {number} b
   * @return {number} dif
   */
  dif: function(a,b){
    return Number(a - b);
  }
};
